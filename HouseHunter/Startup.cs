﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HouseHunter.Startup))]
namespace HouseHunter
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
