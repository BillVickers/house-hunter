Read me:

HouseHunter.azurewebsites.net

This application will allow Customers to submit their property for valuation.

ASP.NET 4.5.2, ASP.NET MVC 5, Entity Framework 6 and Domain Driven Design

System Requirements:

Visual Studio Enterprise 2015 with Update 1
.NET Framework 4.0, 4.5, 4.5.2 and 4.6


This ASP.NET MVC application uses features like:

1) Code First Migrations
2) Entity Framework and LINQ
3) Razor view engine
4) Custom Membership Provider pointing to your own database users table.
5) Partial views and partial actions
6) Html Helpers
7) Data Annotation validation
8) Attribute Routing
9) Querying View Models

  
The Author:

Bill Vickers

TrueEngineers.net

bill-vickers@att.net
12 Cobia St, Ponte Vedra Beach, FL 32082
904-412-9526
LinkedIn:  https://www.linkedin.com/in/william-vickers-624b1078

EDUCATION
University of North Florida, Jacksonville, FL
•	BS in Computer and Information Science	Expected Graduation: Spring 2016
•	BA in Education 				1997



  
  